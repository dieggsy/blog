+++
title = "Hello, world"
date = 2017-02-08
slug = "hello-world"
tags = ["meta"]
draft = false
+++

Hey there. Maybe it's about time I got this site up and running. I've got some
ideas for posts, but I've yet to master even running this blog without messing
everything up, so let's try a quick test post.


## Things I do {#things-i-do}

I'm an Aeropsace Engineering student, though I probably won't talk about that
much here. In my spare time, I like to mess around with Python, Emacs, and the
command line. I recently started looking into Haskell, though I doubt that'll
amount to anything. Anyway, let's see if code snippets are working.

Python:

```python
def my_function(my, arguments):
    print(my, arguments)
```

Emacs-lisp:

```elisp
(defun my-function (arg)
  (when (stringp arg) (message arg)))
```

I play some music, too. I've played violin the longest, and I've played
mandolin for a while. I dabble in guitar.


## Things I like {#things-i-like}

I'm into classic rock, jazz, and some heavier rock, salsa, and bachata. I like
EDM well enough, when I'm in the mood.

My favorite superhero is [Nightwing](https://en.wikipedia.org/wiki/Nightwing). tl;dr: He was the original Robin, now he's
pure awesome. I pretty much only read DC Comics because it's what I got into
first, though I'm interested in reading some Marvel, Dark Horse, and indie
stuff too.


## Ciao {#ciao}

That about sums this post up, I think. We'll see how it turns out. I'm still
surprised this domain name wasn't already taken. I hope I can do it justice,
eventually.
