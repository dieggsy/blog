+++
title = "Trying out Iosevka font"
date = 2017-02-15
slug = "trying-out-iosevka-font"
tags = ["meta", "font"]
draft = false
+++

I've been using the [Input font](http://input.fontbureau.com/) for a while now, and I was pretty sure I had
found my ultimate font. It's really neat and consistent (the boxy-ness grows on
you), and very customizable. However, I was recently browsing [this thread](https://www.reddit.com/r/emacs/comments/5twcka/which_font_do_you_use/) on
the Emacs subreddit in which someone mentioned [Iosevka](https://github.com/be5invis/Iosevka) as their preferred font.
On first impression I sort of just brushed it away, but when I went back and
looked at it, I realized it had similar customizability to Input _and_ was open
source. Well, now I had to give it a shot.


## Installing the font {#installing-the-font}

For customizability, building Iosevka from source is probably the best option.
The directions for macOS on the repo are ok, but let's go through them anyway,
just to be sure.

-   Install [nodejs](https://nodejs.org/en/), [ttfautohint](https://www.freetype.org/ttfautohint/), and [otfcc](https://github.com/caryll/otfcc).
    -   On macOS:

        ```sh
        brew install node ttfautohint
        brew tap caryll/tap
        brew install otfcc-mac64
        ```
    -   On Linux, it's a bit longer:
        1.  [Install nodejs](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)
        2.  Install ttfautohint

            ```sh
            sudo apt-get install ttfautohint
            ```
        3.  Get premake executable

            ```sh
            wget https://github.com/premake/premake-core/releases/download/v5.0.0-alpha11/premake-5.0.0-alpha11-linux.tar.gz
            tar -xf premake-5.0.0-alpha11-linux.tar.gz
            sudo cp premake5 /usr/local/bin/premake5 # Or wherever you want
            ```
        4.  Build otfcc

            ```sh
            git clone https://github.com/caryll/otfcc
            cd otfcc
            premake5 gmake
            cd build/make
            make config=release_x64
            cd ../../
            cd bin/release-x64
            mv otfccbuild /usr/local/bin # or wherever
            mv otfccdump /usr/local/bin # or wherever
            ```
-   Next, install necessary libs with `npm install`.

-   Neat! Now you can use the instructions under "Build Your Own Style" to
    customize the font to your liking. To make it as similar to my Input
    configuration as possible, I ran the following:

    ```sh
    make custom-config set=input design='v-l-tailed v-i-hooky v-a-singlestorey v-zero-dotted v-asterisk-low v-g-singlestorey v-brace-straight'
    ```

-   After that, you can run `make custom set=input` and the `.ttf` files will be
    generated in the directory `dist/iosevka-input`, which you can then just drag
    into the "User" section of Font Book on macOS, or into
    `$home/.local/share/fonts/` in Linux (and then run `fc-cache -f -v`).


## Generating and using the web fonts {#generating-and-using-the-web-fonts}

Though I still wasn't completely sold on the font, I thought It would be a good
learning experience to try and use the custom web fonts for my blog. Generating
the web fonts is similar, except that we use `make custom-web set=input` instead
of `make custom`. You need a couple more things to run this, though: `sfnt2woff`
and `woff2_compress`

-   macOS

    ```sh
    brew tap bramstein/webfonttools
    brew install sfnt2woff woff2
    ```
-   Linux
    -   `sfnt2woff`

        ```sh
        sudo apt-get install woff-tools
        ```

    -   Next, you can install `woff2` utilities as follows (don't worry about the
        errors, as long as the executable `woff2_compress` is generated, you _should_ be
        good, I think):

        ```sh
        git clone --recursive https://github.com/google/woff2
        cd woff2
        make clean all
        sudo mv woff2_compress /usr/local/bin
        sudo mv woff2_decompress /usr/local/bin
        ```

Now you can run `make custom-web set=input` in the Iosevka directory. The `.woff` files are
generated in `dist/iosevka-input/web`.

Next we need to make the appropriate files available to our website. Since I'm
using Gitlab Pages with Hugo, I made a directory in `themes/Lanyon/static/fonts`
and moved four files there:

```sh
cp iosevka-input-regular.woff ~/blog/themes/Lanyon/static/fonts
cp iosevka-input-italic.woff ~/blog/themes/Lanyon/static/fonts
cp iosevka-input-bold.woff ~/blog/themes/Lanyon/static/fonts
cp iosevka-input-bolditalic.woff ~/blog/themes/Lanyon/static/fonts
```

Then, we add a bit of CSS to `themes/lanyon/static/css/poole.css`:

```css
@font-face{
    font-family: "Iosevka";
    src: url("../fonts/iosevka-input-regular.woff") format('woff');
}

@font-face{
    font-family: "Iosevka";
    src: url("../fonts/iosevka-input-italic.woff") format('woff');
    font-style: italic;
}

@font-face{
    font-family: "Iosevka";
    src: url("../fonts/iosevka-input-bold.woff") format('woff');
    font-weight: bold;
}

@font-face{
    font-family: "Iosevka";
    src: url("../fonts/iosevka-input-bolditalic.woff") format('woff');
    font-weight: bold;
    font-style: italic;
}
```

After adding Iosevka to the `font-family` attribute under `mono, tt, code, pre` and
building the site, this font is now used for code on the blog.


## Doubts and further tweaks {#doubts-and-further-tweaks}

I'm still a bit unsure how I feel about the font. I really like its
customizability though, and I appreciate that it's open source, so I'm going to
try to stick with it for about a week to try and get used to it and see how it
goes. Overall, I think it makes a nice alternative to Input that's just as
clean and self-consistent. My main concern is how horizontally _squashed_ the
font looks, but there's more parameters to play around with in the
`parameters.toml` file, including character width, line height, and more!
