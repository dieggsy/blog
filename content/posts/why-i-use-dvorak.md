+++
title = "Why I use Dvorak"
date = 2017-04-16
slug = "why-i-use-dvorak"
tags = ["typing", "dvorak", "keyboard"]
draft = false
+++

You might have heard of the [Dvorak](https://en.wikipedia.org/wiki/Dvorak_Simplified_Keyboard) keyboard layout a couple times. You may have
heard someone prattle on about how much more efficent it is and bla bla bla-
seriously, nobody cares about your keyboard layout, dude. And I sympathize. I
warn you this post may begin to sound like that towards the end. Instead of
prattling, however, I'll try to just share my experience and let you come up with
our own thoughts on the matter.


## Qwerty background {#qwerty-background}

I originally learned to type with a Querty keyboard. Makes sense, since this is
what they taught us in school, and when I eventually got my own computer it was
inevitably already laid out in Qwerty. Eventually, I got pretty _ok_ at it, an
above average typer among my friends at around 120 wpm. Sure, not _earth
shatteringly_ fast, but quick enough. Only "issue" was I typed kind of
weirdly—I'd use almost all my fingers on the left hand, and one finger on the
right. Given the speed I was typing at, this was fine, it just didn't feel
optimal to me, nor could I type without looking at the keyboard.


## Change to Dvorak {#change-to-dvorak}

One summer internship, I noticed my colleague set up a different keyboard
layout on one of the work computers. When I asked him about it, I got the usual
spiel about how it's more efficient as it keeps you on the home row and
encourages alternation between hands. I also noticed his physical keyboard was
still in the Qwerty layout. He mentioned that doing this forced him to learn to
touch type, since he couldn't rely on his eyes at all to type.

Acting on my annoyance at my sub-optimal usage of the keyboard space, as well
as intrigued by the argument of efficiency, I decided learning a completely new
keboard layout was a good way to finally learn to touch type.

The initial process was _grueling_ work—wherever I went I carried a little print
out of the Dvorak layout. This gave me a reference as I learned, without
creating the visual/motor associations to an actual physical keyboard. After
the summer was over, I felt relatively comfortable with the layout and could
happily type without looking at my keyboard, using my fingers for the "right"
keys[^fn:1].


## Things to consider {#things-to-consider}

Right off the bat: I'm not a faster typer in Dvorak that I was at my peak with
Querty. I'm at something like 100 wpm. Granted, it's been almost two years, and
I've seen steady improvement since I started. There's still time to get better.
What the switch did give me was more of a sense of comfort with the keyboard—I
almost never look down at it these days (I'll conquer you one day, number keys)
and for the most part my fingers only move to comfortable positions without
much travel. These things are not advantages of the Dvorak layout, but rather
advantages of forcing myself to learn to type properly as a result of the
switch.

I would like you to consider the following, however. These are [keyboard
heatmaps](https://www.patrick-wied.at/projects/heatmap-keyboard/) of the entire text of this post, first in Querty:

{{<figure src="/images/qwerty_heatmap.png">}}

And then in Dvorak:

{{<figure src="/images/dvorak_heatmap.png">}}

As you can see, the Dvorak heatmap does actually stay in the home row better,
reducing finger travel. However, you don't have to use Dvorak for this—here's
the same heatmap for Colemak:

{{<figure src="/images/colemak_heatmap.png">}}

Colemak is supposed to be a more Qwerty-familiar layout that achieves a similar
purpose as Dvorak: minimal movement and alternation. I might even recommend
Colemak over Dvorak to others familiar with Qwerty. The reason I went with
Dvorak (well, partially after the fact, since I didn't know about Colemak then)
is that a more drastic change, meaning fewer similar key placements,
_necessitated_ my learning to touch type. And I'm pretty happy with that
decision, to be honest.

[^fn:1]: Despite my annoyance about my own finger placement/usage for the keyboard, I strongly believe that the "right" finger placement is still just some norm that doesn't account for subjective experience - in other words, just type however the heck works for you.
